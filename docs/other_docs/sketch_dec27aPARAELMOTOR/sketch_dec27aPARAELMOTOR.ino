//#include <LiquidCrystal.h>

#include <IRremote.h>			// importa libreria IRremote
#define Boton_1 0xF30CFF00	// Número 1 en el control
#define Boton_2 0xE718FF00	// Número 2 en el control
#define Boton_3 0xA15EFF00	// Número 3 en el control
#define Boton_4 0xF708FF00  // Número 4 en el control
#define Boton_5 0xE916FF00  // Botón de ok en el control

// Declaración de las variables
int RECEPTOR = 7; 			// sensor KY-022 a pin digital 11
IRrecv irrecv(RECEPTOR);
//decode_results codigo;
//const int pinSalida = 4;
int LEDROJO = 2;			// componente rojo de LED RGB a pin 2
int LEDVERDE = 3;			// componente verde de LED RGB a pin 3
int LEDAZUL = 4;
int IN1 = 11; // los IN es para controlar el sentido de giro
int IN2 = 10; 
int ENA = 6; // El enable habilita al motor que uno escoja, ayuda para el PWM si es requerido

// Para un segundo motor
// int IN3 = 9; // los IN es para controlar el sentido de giro
// int IN4 = 8; 
// int ENB = 5; // El enable habilita al motor que uno escoja, ayuda para el PWM si es requerido

void setup() {
  Serial.begin(9600);			// inicializa comunicacion serie a 9600 bps
  IrReceiver.begin(RECEPTOR, DISABLE_LED_FEEDBACK);
  //IrReceiver.begin(SENSOR, DISABLE_LED_FEEDBACK);	// inicializa recepcion de datos
  pinMode(LEDROJO, OUTPUT);		// pin 2 como salida
  pinMode(LEDAZUL, OUTPUT);		// pin 4 como salid
  pinMode(LEDVERDE, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(ENA, OUTPUT);

  // pinMode(IN3, OUTPUT);
  // pinMode(IN4, OUTPUT);
  // pinMode(ENB, OUTPUT);
  
  } 

void loop() {
  if (IrReceiver.decode()) {				// si existen datos ya decodificados
    Serial.println(IrReceiver.decodedIRData.decodedRawData, HEX);	// imprime valor en hexadecimal en monitor

    // PRIMER CONDICIONAL PARA GIRO AL 25%
    if (IrReceiver.decodedIRData.decodedRawData == Boton_1) {
      analogWrite(ENA, 68);
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(LEDROJO, HIGH);
      digitalWrite(LEDVERDE, LOW);
      digitalWrite(LEDAZUL, LOW);
    }	
    
    // SEGUNDO CONDICIONAL PARA GIRO AL 50%
    if (IrReceiver.decodedIRData.decodedRawData == Boton_2) {
      analogWrite(ENA, 127);
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(LEDROJO, LOW);
      digitalWrite(LEDVERDE, HIGH);
      digitalWrite(LEDAZUL, LOW);
    }

    // TERCER CONDICIONAL PARA GIRO AL 75%
    if (IrReceiver.decodedIRData.decodedRawData == Boton_3) {
      analogWrite(ENA, 191);
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(LEDROJO, LOW);
      digitalWrite(LEDVERDE, LOW);
      digitalWrite(LEDAZUL, HIGH);
    }

    // CUARTO CONDICIONAL PARA GIRO AL 100%
    if (IrReceiver.decodedIRData.decodedRawData == Boton_4) {
      analogWrite(ENA, 255);
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(LEDROJO, HIGH);
      digitalWrite(LEDVERDE, HIGH);
      digitalWrite(LEDAZUL, HIGH);
      
    }

    // QUINTO CONDICIONAL PARA DETENER GIRO
    if (IrReceiver.decodedIRData.decodedRawData == Boton_5) {
      // analogWrite(ENA, 255);
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(LEDROJO, LOW);
      digitalWrite(LEDVERDE, LOW);
      digitalWrite(LEDAZUL, LOW);
    }

    IrReceiver.resume();				// resume la adquisicion de datos


  }
  delay (100);						// breve demora de 100 ms.
}